module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass:{
			options: {
				noCache:'',
				sourcemap: 'none',
				style:'expanded',
				spawn:false
			},
		dist: {files: {'css/main.css': 'sass/main.scss',} }
		},
		autoprefixer: {
        dev: {
          options: { browsers: ['last 10 version'] },
          src: "css/main.css"
        }
      },
	   cssmin: {
			options:{spawn:false,},
			compress: {	files: {'css/main.min.css': 'css/main.css'}}
		},
		pug:{
			compile:{
				options:{
					data:{debug:false},
					pretty:true,
					spawn:false,
				},
				files:{'index.html' :['pug/index.pug']}
			}
		},
		/*browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html'
                    ]
                },
                options: {
					spawn:false,
                    watchTask: true,
                    server: './'
                }
            }
        },*/
		watch: {
			grunt: { files: ['Gruntfile.js'] },
			options:{
				spawn:false,
				livereload:true
			},
			sass:{
				files:['**/*.scss'],
				tasks:['sass','cssmin','autoprefixer']
			},
			styles: {
				files: ['css/main.css'],
				tasks: ['cssmin','autoprefixer']
			},
			pug:{
				files:['**/*.pug'],
				tasks:['pug']
			}
		}
	});
	//Load plugins
	 require('load-grunt-tasks')(grunt);
	 //Custom tasks
	 grunt.registerTask('mincss',['cssmin']);
	 grunt.registerTask('default',['watch','sass','autoprefixer','pug']);
};