// 0. If using a module system, call Vue.use(VueRouter)

// 1. Define route components.
// These can be imported from other files
const breakfast = { template: '<div class="wrapper"><h2 class="menuHeader">Appetizers</h2><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage.jpg" alt=""/><h2>Chicken wings</h2><p>It’s all about flavour and fun, fun, fun. Toss fried chicken with a finger-licking sauce spiked with chilli and fresh ginger.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage2.jpg" alt=""/><h2>Dumplings</h2><p>Small dumplings filled with spiced lamb. Tossed in a tomato sauce and served with sumac-flavoured Greek yoghurt.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage3.jpg" alt=""/><h2>French onion soup</h2><p>French onion soup is deeply savoury and hearty - and cheesy croutons make it extra special!</p></div></div>' }
const lunch = { template: '<div class="wrapper"> <h2 class="menuHeader">Entrees</h2><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage.jpg" alt=""/><h2>Atlantic Salmon</h2><p>Have dinner on the table super fast with clever shortcuts and this easy salmon and Greek rice salad.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage2.jpg" alt=""/><h2>Fish tacos</h2><p>Fish is a healthy, low-fat taco filling, and it tastes absolutely delicious.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage3.jpg" alt=""/><h2>Sesame Tuna</h2><p>Sesame seeds form a tasty crust and are rich in calcium, copper and magnesium.</p></div></div></div>' }
const drinks = { template: '<div class="wrapper"> <h2 class="menuHeader">Main Course</h2><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage.jpg" alt=""/><h2>Chicken wings</h2><p>It’s all about flavour and fun, fun, fun. Toss fried chicken with a finger-licking sauce spiked with chilli and fresh ginger.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage2.jpg" alt=""/><h2>Chicken wings</h2><p>It’s all about flavour and fun, fun, fun. Toss fried chicken with a finger-licking sauce spiked with chilli and fresh ginger.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage3.jpg" alt=""/><h2>Chicken wings</h2><p>It’s all about flavour and fun, fun, fun. Toss fried chicken with a finger-licking sauce spiked with chilli and fresh ginger.</p></div></div>' }
const desserts = { template: '<div class="wrapper"> <h2 class="menuHeader">Salads</h2><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage.jpg" alt=""/><h2>Lobster salad</h2><p>A simple lobster salad with butter and just a hint of mayonnaise so that you can still taste the sweet lobster meat.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage2.jpg" alt=""/><h2>Chicken & orange salad</h2><p>With juicy oranges and a yoghurt dressing, this chicken salad is a treat on any spring table.</p></div></div><br><div class="menuContent"><div class="menuContent__item"><img src="img/menuimage3.jpg" alt=""/><h2>Winter salad</h2><p>This is a simple winter salad,  fresh and crisp. </p></div></div></div>' }
// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// Vue.extend(), or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/breakfast', component: breakfast },
  { path: '/lunch', component: lunch },
  { path: '/drinks', component: drinks },
  { path: '/desserts', component: desserts }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes
})

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
  router
}).$mount('#app')
Vue.component('todo-item', {
  template: `
    <li>
      {{ title }}
      <button v-on:click="$emit('remove')">X</button>
    </li>
  `,
  props: ['title']
})
new Vue({
  el: '#todo-list-example',
  data: {
    newTodoText: '',
    todos: [
      'Do the dishes',
      'Take out the trash',
      'Mow the lawn'
    ]
  },
  methods: {
    addNewTodo: function () {
      this.todos.push(this.newTodoText)
      this.newTodoText = ''
    }
  }
})